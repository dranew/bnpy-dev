from setuptools import setup, find_packages

setup(
    name='bnpy',
    packages=find_packages(),
    package_data={'bnpy': ['config/*conf*']},
)
